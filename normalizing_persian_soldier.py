# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas


df = pandas.read_excel('C:\\Users\\Mark\\Desktop\\GLMM\\clean_persian_text_soldier.xlsx')
#this did not work to remove the quotes
df['text'] = df['text'].str.replace('”', '')

df['text'] = df['text'].str.replace('“', '')

df['text'] = df['text'].str.replace('«', '')

df['text'] = df['text'].str.replace('»', '')
 
from hazm import *

normalizer = Normalizer()

df['normalized_tweets'] = df['text'].apply(normalizer.normalize)

stemmer = Stemmer()

writer = pandas.ExcelWriter('normalized_persian_soldier.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
df.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()